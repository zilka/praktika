package praktika.caption;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zilvinas.vinskas on 7/21/2016.
 */
@Service
public final class CaptionService {
    final static private Map<String,String> captionMap = getCaptionMap();

    private static Map<String, String> getCaptionMap() {
        Map<String, String> captions = new HashMap<>();

        captions.put("MainMenuCaption",
                "Hello!\n" +
                "\n" +
                "1. New task\n" +
                "2. Existing tasks\n" +
                "e. Exit\n");

        captions.put("NewTaskMenuCaption",
                "Choose a task's type:\n" +
                "\n"+
                "1. Reminder\n"+
                "2. Meeting\n"+
                "3. ReservedTime time\n"+
                "\n"+
                "b. Back\n"+
                "e. Exit\n");

        captions.put("TaskListMenuCaption", "How would you like your tasks list to be sorted?\n" +
                "\n" +
                "1. By Name\n" +
                "2. By Start time\n" +
                "3. Reminders by start time\n" +
                "4. Meetings by start time\n" +
                "5. ReservedTime time by start time\n" +
                "\n"+
                "b. Back\n"+
                "e. Exit\n");
        captions.put("AfterListingMenuCaption", "Enter Task's ID number\n" +
                "or\n" +
                "b. Back\n" +
                "e. Exit\n");

        captions.put("SelectTaskId", "Enter number of task.\n");

        captions.put("SpecificTaskMenuCaption", "Choose option.\n" +
                "1. Update\n" +
                "2. Delete\n" +
                "b. Back\n" +
                "e. Exit\n");

        captions.put("soon,", "Coming soon\n");

        captions.put("ReminderCreated", "New REMINDER has been created\n"); //maybe

        captions.put("MeetingCreated", "New MEETING has been created\n");  //maybe

        captions.put("ReservedCreated", "New RESERVED TIME has been created\n"); //maybe

        captions.put("InvalidInputCaption", "Your input is Invalid, try again.\n");

        captions.put("EnterInputCaption", "Enter number: ");

        captions.put("WrongIdCaption", "Sorry. Can not find Task by this ID");

        captions.put("ErrorDelete", "Could not delete this task.");

        captions.put("ErrorUpdate", "Could not update this task.");

        captions.put("ErrorCreate", "Could not create this task. Probably wrong inputs.");

        captions.put("SelectCaption", "b. Back\n");

        captions.put("ExitCaption", "e. Exit\n");
        captions.put("EmptyList", "There are no task you are looking for");

        return captions;
    }

    public String getCaption(String key) {
        return captionMap.get(key);
    }






}
