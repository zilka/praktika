package praktika.userInput;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/25/2016.
 */
@Component
public class TaskInputFileReader implements DataInputReader {


    public List<String> readFileToList() throws IOException {
        List<String> list = new ArrayList<>();
        FileIOToolkit fileIOToolkit = new FileIOToolkit();
        BufferedReader bufferedReader = fileIOToolkit.makeReader(FILENAME);
        String buffer;
        while ((buffer = bufferedReader.readLine()) != null){
            list.add(buffer);
        }
        return list;
    }

}
