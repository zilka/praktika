package praktika.userInput;

import org.springframework.stereotype.Component;

import java.util.Scanner;

/**
 * Created by zilka on 2016-07-21.
 */
@Component
public class ConsoleInputScanner implements InputScanner {

    public String getInput(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}