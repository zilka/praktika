package praktika.userInput;

/**
 * Created by zilvinas.vinskas on 7/21/2016.
 */
public interface InputScanner {
    String getInput();
}
