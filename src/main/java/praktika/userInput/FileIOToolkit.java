package praktika.userInput;

import org.springframework.stereotype.Component;

import java.io.*;

/**
 * Created by zilka on 2016-07-19.
 */
@Component
public class FileIOToolkit {

    public FileIOToolkit() {
    }

    public BufferedReader makeReader(File inputFileName) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(inputFileName));
        return br;
    }

    public BufferedWriter makeWriter(String outputFileName) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFileName));
        return bw;
    }

}
