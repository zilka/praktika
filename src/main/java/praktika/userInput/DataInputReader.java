package praktika.userInput;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
public interface DataInputReader {
    File FILENAME = new File("inp.txt");

    List<String> readFileToList() throws IOException;
}
