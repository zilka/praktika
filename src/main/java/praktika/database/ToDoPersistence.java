package praktika.database;

import praktika.tasks.dataStructures.Task;

import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/28/2016.
 */
public interface ToDoPersistence {

    void insertTask(Task task);

    Task getTask(int id); //Don't know if necessary

    void loadAllTasks(List<Task> mainTaskList);

    void updateTask(Task task);

    void deleteTask(Task task);

    void deleteAllTasks();

    void printDb();
}
