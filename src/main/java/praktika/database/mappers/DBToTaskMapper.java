package praktika.database.mappers;

import praktika.tasks.dataStructures.Task;
import praktika.tasks.factories.SimpleTaskFactory;
import praktika.tasks.factories.TaskType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by zilvinas.vinskas on 7/29/2016.
 */
@Service
public class DBToTaskMapper implements Mapper<Task> {

    @Autowired
    private SimpleTaskFactory taskFactory;



    @Override
    public Task map(ResultSet resultSet) {

        try {
            int id = resultSet.getInt("id");
            TaskType type = TaskType.valueOf(resultSet.getString("type"));
            String name = resultSet.getString("name");
            LocalDateTime startTime;
            if (resultSet.getTimestamp("start_time") == null){
                startTime = null;
            } else {
                startTime = resultSet.getTimestamp("start_time").toLocalDateTime();
            }
            Duration duration = Duration.ofMinutes(resultSet.getLong("duration"));
            String location = resultSet.getString("location");
            return taskFactory.makeTask(id, type, name, startTime, duration,location);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
