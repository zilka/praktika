package praktika.database.mappers;

import praktika.tasks.dataStructures.Attendee;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by zilvinas.vinskas on 7/29/2016.
 */
@Service
public class AttendeeMapper implements Mapper {

    @Override
    public Attendee map(ResultSet resultSet){
        try {
            String name = resultSet.getString("name");
            String surname = resultSet.getString("surname");
            return new Attendee(name,surname);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
