package praktika.database.mappers;

import java.sql.ResultSet;

/**
 * Created by zilvinas.vinskas on 7/29/2016.
 */
public interface Mapper<T> {
    T  map(ResultSet resultSet);
}

