package praktika.database;


import praktika.tasks.dataStructures.Attendee;
import praktika.tasks.dataStructures.Meeting;
import praktika.tasks.dataStructures.Task;
import praktika.tasks.factories.SimpleTaskFactory;
import praktika.tasks.factories.TaskType;
import praktika.database.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import static praktika.tasks.factories.TaskType.MEETING;


/**
 * Created by zilvinas.vinskas on 7/28/2016.
 */
@Component
public class DatabasePersistence implements ToDoPersistence {

    private DatabaseProperties properties;

    private static final String INSERT_TASK = "INSERT INTO tasks (type, name, start_time, duration, location) VALUES (?, ?, ?, ?, ?)";
    private static final String GET_TASK = "SELECT * FROM tasks WHERE id = ?";
    private static final String LOAD_ALL_TASKS = "SELECT * FROM tasks";
    private static final String UPDATE_TASK = "UPDATE tasks set name = ?, start_time = ?, duration = ?, location = ? WHERE id = ?";
    private static final String GET_ATTENDEES = "SELECT name,surname FROM meetings_attendees where id = ?";
    private static final String SET_ATTENDEES = "INSERT INTO meetings_attendees (id, name, surname) values (?, ?, ?)";
    private static final String DELETE_ATTENDEES = "DELETE FROM meetings_attendees WHERE id = ?";
    private static final String DELETE_TASK = "DELETE FROM tasks WHERE id = ?";
    private static final String DELETE_ALL_TASKS = "DELETE FROM tasks";
    private static final String SELECT_ALL = "SELECT * FROM ?";


    @Autowired
    Mapper<Attendee> attendeeMapper;

    @Autowired
    Mapper<Task> taskMapper;

    @Autowired
    public DatabasePersistence(DatabaseProperties properties) {
        this.properties = properties;
        try {
            Class.forName(this.properties.getDriver());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    private Connection getConnection() {
        try {
            return DriverManager.getConnection(properties.getUrl(), properties.getUsername(), properties.getPassword());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insertTask(Task task) {
        PreparedStatement statement = null;
        try (Connection connection = getConnection()) {
            statement = connection.prepareStatement(INSERT_TASK,Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, task.getType().name());
            statement.setString(2, task.getName());
            try {
                statement.setTimestamp(3, Timestamp.valueOf(task.getStartTime()));
            } catch (NullPointerException e){
                statement.setTimestamp(3, null);
            }
            statement.setLong(4, task.getDuration().toMinutes());
            statement.setString(5, task.getLocation());
            statement.executeUpdate();
            int id = 0;
            try (ResultSet resultSet = statement.getGeneratedKeys()){
                if (resultSet.next()) {
                    id = resultSet.getInt(1);
                    task.setId(id);
                } else {
                    throw new SQLException();
                }
            }

            if (task.getType() == MEETING){
                insertAttendees(id,((Meeting)task).getAttendees());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
        }
    }


    @Override
    public Task getTask(int id) {
        PreparedStatement statement = null;
        SimpleTaskFactory taskFactory = new SimpleTaskFactory();
        try (Connection conn = getConnection()) {
            statement = conn.prepareStatement(GET_TASK);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {

                id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                TaskType type = TaskType.valueOf(resultSet.getString("type"));
                LocalDateTime startTime = resultSet.getTimestamp("start_time").toLocalDateTime();
                Duration duration = Duration.ofNanos(resultSet.getLong("duration"));
                String location = resultSet.getString("location");
                Task task = taskFactory.makeTask(id, type, name, startTime, duration, location);
                if (type == MEETING) {
                    ((Meeting) task).setAttendees(getAttendeesByMeetingId(id));
                }
                return task;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    @Override
    public void loadAllTasks(List<Task> mainTaskList) {
        PreparedStatement statement = null;
        try (Connection conn = getConnection()) {
            statement = conn.prepareStatement(LOAD_ALL_TASKS);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Task task = taskMapper.map(resultSet);
                if (task.getType() == MEETING){
                    ((Meeting) task).setAttendees(this.getAttendeesByMeetingId(task.getId()));
                }
                mainTaskList.add(task);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
        }
    }



    @Override
    public void updateTask(Task task) {
        PreparedStatement statement = null;
        try(Connection connection = getConnection()){
            statement = connection.prepareStatement(UPDATE_TASK);
            statement.setString(1, task.getName());
            statement.setTimestamp(2, Timestamp.valueOf(task.getStartTime()));
            statement.setLong(3, task.getDuration().toMinutes());
            statement.setString(4, task.getLocation());
            statement.setInt(5, task.getId());
            System.out.println("TASK IDIDIDIDIDID ="+task.getId());
            statement.executeUpdate();
            if (task.getType() == MEETING){
                this.deleteAttendeesByMeetingId(task.getId());
                this.insertAttendees(task.getId(),((Meeting)task).getAttendees());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
        }
    }

    @Override
    public void deleteTask(Task task) {
        PreparedStatement statement = null;
        try (Connection connection = getConnection()) {
            statement = connection.prepareStatement(DELETE_TASK);
            statement.setInt(1, task.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

        @Override
    public void deleteAllTasks() {
        PreparedStatement statement = null;
        try (Connection connection = getConnection()) {
            statement = connection.prepareStatement(DELETE_ALL_TASKS);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    private void insertAttendees(int id, List<Attendee> attendees) {
        PreparedStatement statement = null;
        if (attendees.isEmpty()){
            return;
        }
        try (Connection connection = getConnection()){
            statement = connection.prepareStatement(SET_ATTENDEES);
            for (Attendee attendee : attendees) {
                statement.setInt(1, id);
                statement.setString(2, attendee.getName());
                statement.setString(3, attendee.getSurname());
                statement.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(statement);
        }
    }

    private List<Attendee> getAttendeesByMeetingId(int id) {
        PreparedStatement statement = null;
        List<Attendee> taskList = new ArrayList<>();
        try (Connection conn = getConnection()) {
            statement = conn.prepareStatement(GET_ATTENDEES);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                taskList.add(attendeeMapper.map(resultSet));
            }
            return taskList;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    private void deleteAttendeesByMeetingId(int id){
        PreparedStatement statement = null;
        try (Connection connection = getConnection()){
            statement = connection.prepareStatement(DELETE_ATTENDEES);
            statement.setInt(1,id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }

    }

    private void closeStatement(Statement statement) {
        if (statement != null){
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    // for checking db.

    public void printDb(){
        PreparedStatement statement = null;
        try(Connection connection = getConnection()){
            statement = connection.prepareStatement(SELECT_ALL);
            statement.setString(1,"tasks");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                System.out.println(resultSet.toString());
            }

            System.out.println();

            statement.setString(1,"tasks");
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                System.out.println(resultSet.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.closeStatement(statement);
        }
    }
}
