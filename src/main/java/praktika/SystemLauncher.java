package praktika;

import org.springframework.boot.CommandLineRunner;
import praktika.menu.menuGUI.MapOfMenusService;
import praktika.notifications.TimerImp;
import praktika.database.ToDoPersistence;
import praktika.menu.menuGUI.Menu;
import praktika.notifications.Timer;
import praktika.storing.whileNoDB.TaskList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zilvinas.vinskas on 7/22/2016.
 */

@Service
class SystemLauncher implements CommandLineRunner {
    @Autowired
    private MapOfMenusService mapOfMenusService;
    @Autowired
    private ToDoPersistence toDoPersistence;

    public void run(String... strings) throws Exception {
        toDoPersistence.loadAllTasks(TaskList.getTaskList());
        Timer timerThread = new TimerImp();    //TODO: Look up @Schedule
        timerThread.start();
        Menu mainMenu = mapOfMenusService.getMenu("MainMenu");
        mainMenu.start();
    }
}

//done
