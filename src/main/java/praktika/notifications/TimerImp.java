package praktika.notifications;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by zilvinas.vinskas on 8/1/2016.
 */
@Component
public class TimerImp extends Thread implements Timer{
    @Autowired
    private Thread thread;

    private String threadName = "lala";


    public void run(){
        try {
            Notificator notificator = new Notificator();
            while(true){
                notificator.lookForNotifications();
                Thread.sleep(60000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void start(){
        if (thread == null){
            thread = new Thread(this, threadName);
            thread.start();
        }
    }
}
