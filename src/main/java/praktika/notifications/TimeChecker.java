package praktika.notifications;

import praktika.tasks.dataStructures.Task;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Created by zilvinas.vinskas on 8/1/2016.
 */
@Component
public class TimeChecker {

    boolean isTimeInRange(Task task) {
        LocalDateTime date = task.getStartTime();
        LocalDateTime now = LocalDateTime.now();
        if (date != null && now.isAfter(date.minusHours(1)) && now.isBefore(date.minusMinutes(59))) {
            return true;
        }
        return false;
    }
}