package praktika.notifications;

import praktika.tasks.userOutput.TaskPrinter;
import praktika.storing.whileNoDB.TaskList;
import praktika.tasks.dataStructures.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by zilvinas.vinskas on 8/1/2016.
 */
@Component
public class Notificator {

    @Autowired
    private TaskPrinter taskPrinter;

    public void lookForNotifications(){
        List<Task> mainTaskList = TaskList.getTaskList();
        TimeChecker timeChecker = new TimeChecker();
        for (Task task : mainTaskList) {
            if (timeChecker.isTimeInRange(task)) {
                alert(task);
            }
        }
    }

    private void alert(Task task){
        System.out.println("========================= ALARM =========================");
        System.out.println("====================== 1 HOUR LEFT ======================");
        taskPrinter.printTaskDetails(task);
        System.out.println("=========================================================");
    }
}
