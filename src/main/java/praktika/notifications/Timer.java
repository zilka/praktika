package praktika.notifications;

/**
 * Created by zilvinas.vinskas on 8/1/2016.
 */
public interface Timer {
    void start();
}
