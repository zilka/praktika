package praktika.storing.manipulations;

import praktika.tasks.dataStructures.Task;

import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/26/2016.
 */
public interface TaskFinder {
    Task findTaskById(List<Task> mainTaskList, int id);
}
