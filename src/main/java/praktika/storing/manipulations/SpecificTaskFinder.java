package praktika.storing.manipulations;

import praktika.tasks.dataStructures.Task;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/26/2016.
 */
@Component
public class SpecificTaskFinder implements TaskFinder {

    @Override
    public Task findTaskById(List<Task> mainTaskList, int id) {
        Task tempTask;
        for (int i = 0; i < mainTaskList.size(); i++) {
            tempTask = mainTaskList.get(i);
            if (id == tempTask.getId()){
                return tempTask;
            }
        }
        return null;
    }
}
