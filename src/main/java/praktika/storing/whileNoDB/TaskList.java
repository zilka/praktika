package praktika.storing.whileNoDB;

import praktika.tasks.dataStructures.Task;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
@Service
public class TaskList {
    private static List<Task> taskList = new ArrayList<>();
    private static int flaggedTaskId = 0;

    public static int getFlaggedTaskId() {
        return flaggedTaskId;
    }

    public static void setFlaggedTaskId(int flaggedTaskId) {
        TaskList.flaggedTaskId = flaggedTaskId;
    }

    public static List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList){
        TaskList.taskList = taskList;
    }

    public Task getTaskById(int id){
        for (Task task : taskList) {
            if (task.getId() == id){
                return task;
            }
        }
        return null;
    }

    public Task getTaskById(){
        for (Task task : taskList) {
            if (task.getId() == flaggedTaskId){
                return task;
            }
        }
        return null;
    }


}