package praktika.storing.whileNoDB;

import praktika.tasks.dataStructures.Task;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;

/**
 * Created by zilka on 2016-07-21.
 */
@Component
public class TaskListSerializationService {
    public void saveListToFile(List<Task> taskList, String fileName) throws IOException {
        FileOutputStream fos = new FileOutputStream(fileName);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(taskList);
        oos.close();
        fos.close();

        //greiciausiai neveiks
    }

    public List<Task> loadListFromFile(String fileName) throws IOException, ClassNotFoundException {
        List<Task> taskList;
        FileInputStream fis = new FileInputStream(fileName);
        ObjectInputStream ois = new ObjectInputStream(fis);
        taskList = (List<Task>) ois.readObject(); //(*1)
        return taskList;

        //greiciausiai neveiks. reik issiaiskinti (*1) kaip istraukti pilna lista.
    }
}

//Reik kazkaip iskelti tai
