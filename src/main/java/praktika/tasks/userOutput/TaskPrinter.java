package praktika.tasks.userOutput;

import praktika.date.data.manipulations.DateTransformator;
import praktika.tasks.dataStructures.Attendee;
import praktika.tasks.dataStructures.Meeting;
import praktika.tasks.dataStructures.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static praktika.date.validators.DateOverlapValidator.DATE_FORMAT;
import static praktika.tasks.factories.TaskType.MEETING;

/**
 * Created by zilvinas.vinskas on 7/25/2016.
 */
@Component
public class TaskPrinter {
    private DateTransformator dateTransformator;

    @Autowired
    public TaskPrinter(DateTransformator dateTransformator) {
        this.dateTransformator = dateTransformator;
    }



    public void printTaskDetails(Task task){
        System.out.println("== TASK ==");
        System.out.println("ID = "+task.getId());
        System.out.println("Type = "+task.getType());
        System.out.println("Name = "+task.getName());
        System.out.println("Time = "+dateTransformator.formatStringFromDate(task.getStartTime(),DATE_FORMAT) );
        System.out.println("Duration = "+task.getDuration().toMinutes());
        System.out.println("Location = "+task.getLocation());

        if (task.getType() == MEETING){
            List<Attendee> attendeeList = ((Meeting)task).getAttendees();
            if (attendeeList != null){
                System.out.println("== ATTENDEES ==");
                for (int i = 0; i < attendeeList.size(); i++) {
                    System.out.println(i+1+". "+attendeeList.get(i).getName()+" "+attendeeList.get(i).getSurname());
                }
            }
        }
    }

    public void printTasksByName(List<Task> taskList){
        System.out.println("== Existing TASKS ==");
        Task task;
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            System.out.println(task.getId()+" "+task.getName()+" "+task.getType());
        }
        System.out.println("====================");

    }

}
