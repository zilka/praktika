package praktika.tasks.factories;

import praktika.tasks.dataStructures.Meeting;
import praktika.tasks.dataStructures.Reminder;
import praktika.tasks.dataStructures.ReservedTime;
import praktika.tasks.dataStructures.Task;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by zilvinas.vinskas on 7/28/2016.
 */
@Component
public class SimpleTaskFactory {

    public Task makeTask(TaskType taskType) {
        switch (taskType) {
            case REMINDER:
                return new Reminder();
            case MEETING:
                return new Meeting();
            case RESERVED_TIME:
                return new ReservedTime();
            default:
                return null;
        }
    }

    public Task makeTask(int id, TaskType type, String name, LocalDateTime startTime, Duration duration, String location) {
        switch (type) {
            case REMINDER:
                return new Reminder(id, type, name, startTime, duration, location);
            case MEETING:
                return new Meeting(id, type, name, startTime, duration, location);
            case RESERVED_TIME:
                return new ReservedTime(id, type, name, startTime, duration, location);
            default:
                return null;
        }

    }
}