package praktika.tasks.factories;

import praktika.date.data.manipulations.DateTransformator;
import praktika.tasks.dataStructures.Reminder;
import praktika.date.validators.DateOverlapValidator;
import praktika.tasks.dataStructures.Task;
import praktika.userInput.DataInputReader;
import praktika.userInput.TaskInputFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static praktika.date.validators.DateOverlapValidator.DATE_FORMAT;
import static praktika.tasks.factories.TaskType.REMINDER;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
@Component
public class ReminderFactory implements TaskFactory {

    @Autowired
    @Qualifier("reminderDateOverlapValidator")
    DateOverlapValidator dateOverlapValidator;

    @Override
    public Task makeTaskObject() throws IOException {
        Task reminder = new Reminder();
        DataInputReader dataInputReader = new TaskInputFileReader();
        List<String> list = dataInputReader.readFileToList();
        if  (list.size() < 4){
            return null;
        }
        if (list.get(0).equalsIgnoreCase("-")){
            return null;
        }
        String name = list.get(0);

        LocalDateTime startTime;
        if (list.get(1).equalsIgnoreCase("-")){
            startTime = null;
        } else {
            startTime = new DateTransformator().parseDateFromString(list.get(1), DATE_FORMAT);
        }

        Duration duration;
        if (list.get(2).equalsIgnoreCase("-")) {
            duration = Duration.ofMinutes(0);
        } else {
            duration = Duration.ofMinutes(Long.parseLong(list.get(2)));
        }

        String location = list.get(3);

        if (startTime != null) {
            if (!dateOverlapValidator.isDateFree(startTime, duration)) {
                return null;
            }
        }

        reminder.setType(REMINDER);
        reminder.setName(name);
        reminder.setStartTime(startTime);
        reminder.setDuration(duration);
        reminder.setLocation(location);
        return reminder;
    }
}