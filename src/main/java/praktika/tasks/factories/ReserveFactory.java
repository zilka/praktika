package praktika.tasks.factories;

import praktika.date.validators.DateOverlapValidator;
import praktika.tasks.dataStructures.ReservedTime;
import praktika.tasks.dataStructures.Task;
import praktika.date.data.manipulations.DateTransformator;
import praktika.storing.whileNoDB.TaskList;
import praktika.userInput.DataInputReader;
import praktika.userInput.TaskInputFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static praktika.date.validators.DateOverlapValidator.DATE_FORMAT;
import static praktika.tasks.factories.TaskType.RESERVED_TIME;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
@Component
public class ReserveFactory implements TaskFactory {
    @Autowired
    @Qualifier("reservedDateOverlapValidator")
    DateOverlapValidator dateOverlapValidator;

    @Override
    public Task makeTaskObject() throws IOException {
        List<Task> taskList = TaskList.getTaskList();
        Task reserved = new ReservedTime();
        DataInputReader dataInputReader = new TaskInputFileReader();
        List<String> inputDataList = dataInputReader.readFileToList();
        if (inputDataList.size() < 4) {
            return null;
        }
        if (inputDataList.get(0).equalsIgnoreCase("-")) {
            return null;
        }
        if (inputDataList.get(1).equalsIgnoreCase("-")) {
            return null;
        }
        if (inputDataList.get(2).equalsIgnoreCase("-")) {
            return null;
        }
        String name = inputDataList.get(0);
        LocalDateTime startTime = new DateTransformator().parseDateFromString(inputDataList.get(1), DATE_FORMAT);
        Duration duration = Duration.ofMinutes(Long.parseLong(inputDataList.get(2)));
        String location = inputDataList.get(3);

        if (!dateOverlapValidator.isDateFree(startTime, duration)){
            return null;
        }

        reserved.setType(RESERVED_TIME);
        reserved.setName(name);
        reserved.setStartTime(startTime);
        reserved.setDuration(duration);
        reserved.setLocation(location);
        //toDoPersistence.printDb();
        //taskList.add(reserved);
        return reserved;
    }
}
