package praktika.tasks.factories;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
public interface TaskFactoryBuilder {
    TaskFactory makeTaskFactory(TaskType taskType);
}
