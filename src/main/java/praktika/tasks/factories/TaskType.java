package praktika.tasks.factories;

/**
 * Created by zilvinas.vinskas on 7/28/2016.
 */
public enum TaskType {
    REMINDER,
    MEETING,
    RESERVED_TIME
}
