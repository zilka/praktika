package praktika.tasks.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by zilvinas.vinskas on 7/19/2016.
 */
@Component
public class TaskFactoryBuilderImpl implements TaskFactoryBuilder {
    @Autowired
    @Qualifier("reminderFactory")
    TaskFactory reminderFactory;
    @Autowired
    @Qualifier("meetingFactory")
    TaskFactory meetingFactory;
    @Autowired
    @Qualifier("reserveFactory")
    TaskFactory reservedFactory;

    @Override
    public TaskFactory makeTaskFactory(TaskType taskType) {
        switch (taskType) {
            case REMINDER:
                return reminderFactory;
            case MEETING:
                return meetingFactory;
            case RESERVED_TIME:
                return reservedFactory;
            default:
                return null;
        }
    }
}
