package praktika.tasks.factories;


import praktika.date.data.manipulations.DateTransformator;
import praktika.date.validators.DateOverlapValidator;
import praktika.tasks.dataStructures.Attendee;
import praktika.userInput.TaskInputFileReader;
import praktika.tasks.dataStructures.Meeting;
import praktika.tasks.dataStructures.Task;
import praktika.storing.whileNoDB.TaskList;
import praktika.userInput.DataInputReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
@Component
public class MeetingFactory implements TaskFactory {

    @Autowired
    @Qualifier("meetingDateOverlapValidator")
    DateOverlapValidator dateOverlapValidator;

    @Override
    public Task makeTaskObject() throws IOException {
        List<Task> taskList = TaskList.getTaskList();
        Task meeting = new Meeting();
        DataInputReader dataInputReader = new TaskInputFileReader();
        List<String> inputDataList = dataInputReader.readFileToList();
        if (inputDataList.size() < 4) return null;
        if (inputDataList.get(0).equalsIgnoreCase("-")) {
            return null;
        }
        if (inputDataList.get(1).equalsIgnoreCase("-")) {
            return null;
        }
        if (inputDataList.get(2).equalsIgnoreCase("-")) {
            return null;
        }
        String name = inputDataList.get(0);
        LocalDateTime startTime = new DateTransformator().parseDateFromString(inputDataList.get(1), DateOverlapValidator.DATE_FORMAT);
        Duration duration = Duration.ofMinutes(Long.parseLong(inputDataList.get(2)));
        String location = inputDataList.get(3);

        if (!dateOverlapValidator.isDateFree(startTime, duration)){
            return null;
        }

        meeting.setType(TaskType.MEETING);
        meeting.setName(name);
        meeting.setStartTime(startTime);
        meeting.setDuration(duration);
        meeting.setLocation(location);

        List<Attendee> attendeeList = new ArrayList<>();
        String[] splitter;
        for (int i = 4; i < inputDataList.size() ; i++) {
            splitter = inputDataList.get(i).split(" ");
            attendeeList.add(new Attendee(splitter[0],splitter[1]));
        }
        ((Meeting)meeting).setAttendees(attendeeList);
        //taskList.add(meeting);
        return meeting;
    }
}
