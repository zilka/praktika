package praktika.tasks.factories;

import praktika.tasks.dataStructures.Task;

import java.io.IOException;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
public interface TaskFactory{
    Task makeTaskObject() throws IOException;
    //TODO validate input by class requirements
    //TODO Save task object to list
}
