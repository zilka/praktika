package praktika.tasks.dataStructures;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Created by zilvinas.vinskas on 7/19/2016.
 */
public class Attendee implements Serializable{
    private String name;
    private String surname;


    public Attendee(String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        surname = surname;
    }
}


