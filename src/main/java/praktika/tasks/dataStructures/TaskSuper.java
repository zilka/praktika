package praktika.tasks.dataStructures;

import praktika.tasks.factories.TaskType;

import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by zilvinas.vinskas on 7/18/2016.
 */
public class TaskSuper implements Task, Serializable {

    private int id;
    private TaskType type;
    private String name;
    private LocalDateTime startTime;
    private Duration duration;
    private String location;


    public TaskSuper(int id, TaskType type, String name, LocalDateTime startTime, Duration duration, String location){
        this.id = id;
        this.type = type;
        this.name = name;
        this.startTime = startTime;
        this.duration = duration;
        this.location = location;
    }

    public TaskSuper(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
