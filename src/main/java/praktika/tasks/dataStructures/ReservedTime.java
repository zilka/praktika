package praktika.tasks.dataStructures;

import praktika.tasks.factories.TaskType;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by zilvinas.vinskas on 7/19/2016.
 */
public class ReservedTime extends TaskSuper implements Task, Serializable {

    public ReservedTime(){}

    public ReservedTime(int id, TaskType type, String name, LocalDateTime startTime, Duration duration, String location){
        super(id,type,name,startTime,duration,location);
    }
}
