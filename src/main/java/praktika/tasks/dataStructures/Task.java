package praktika.tasks.dataStructures;

import praktika.tasks.factories.TaskType;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
public interface Task {
    TaskType getType();

     void setType(TaskType type);

    String getName();

    void setName(String name);

    LocalDateTime getStartTime();

    void setStartTime(LocalDateTime startTime);

    Duration getDuration();

    void setDuration(Duration duration);

    public String getLocation();

    void setLocation(String location);


    int getId();

    void setId(int id);
}
