package praktika.tasks.dataStructures;

import praktika.tasks.factories.TaskType;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/19/2016.
 */
public class Meeting extends TaskSuper implements Task, Serializable{

    private List<Attendee> attendees;

    public Meeting(){}
    public Meeting(int id, TaskType type, String name, LocalDateTime startTime, Duration duration, String location) {
        super(id,type,name,startTime,duration,location);
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }


    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

}
