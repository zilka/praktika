package praktika.tasks.dataStructures;

import praktika.tasks.factories.TaskType;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by zilvinas.vinskas on 7/19/2016.
 */
public class Reminder extends TaskSuper implements Task {
    public Reminder(){}
    public Reminder(int id, TaskType type, String name, LocalDateTime startTime, Duration duration, String location){
        super(id,type,name,startTime,duration,location);
    }
}
