package praktika.tasks.manipulations;

import praktika.database.ToDoPersistence;
import praktika.tasks.dataStructures.Task;
import praktika.storing.whileNoDB.TaskList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/26/2016.
 */
@Component
public class TaskEraser {


    @Autowired
    private TaskList tl;
    @Autowired
    private ToDoPersistence toDoPersistence;

    public boolean tryEraseTask(){
        List<Task> mainTaskList = tl.getTaskList();
        int id = TaskList.getFlaggedTaskId();
        for (int i = 0; i < mainTaskList.size() ; i++) {
            if  (mainTaskList.get(i).getId() == id){
                toDoPersistence.deleteTask(mainTaskList.get(i));
                mainTaskList.remove(i);
                TaskList.setFlaggedTaskId(0);
                return true;
            }
        }
        return false;
    }
}
