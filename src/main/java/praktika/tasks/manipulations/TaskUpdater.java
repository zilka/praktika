package praktika.tasks.manipulations;


import praktika.database.ToDoPersistence;
import praktika.storing.manipulations.TaskFinder;
import praktika.tasks.factories.TaskFactory;
import praktika.tasks.dataStructures.Task;
import praktika.tasks.factories.TaskFactoryBuilder;
import praktika.storing.whileNoDB.TaskList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by zilvinas.vinskas on 7/26/2016.
 */
@Component
public class TaskUpdater {
    @Autowired
    private TaskFactoryBuilder taskFactoryBuilder;
    @Autowired
    private TaskFinder taskFinder;
    @Autowired
    private ToDoPersistence toDoPersistence;


    public boolean tryUpdateTask() throws IOException {
        Task taskOut = taskFinder.findTaskById(TaskList.getTaskList(), TaskList.getFlaggedTaskId());
        TaskFactory taskFactory = taskFactoryBuilder.makeTaskFactory(taskOut.getType());
        Task taskIn = taskFactory.makeTaskObject();
        if (taskIn == null){
            return false;
        } else {
            taskIn.setId(taskOut.getId());
            toDoPersistence.updateTask(taskIn);
            TaskList.getTaskList().remove(taskOut);
            TaskList.getTaskList().add(taskIn);
        }





        return true;
    }


}
