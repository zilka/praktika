package praktika.date.validators;

import praktika.tasks.dataStructures.Task;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by zilka on 2016-07-26.
 */
@Component
public class BasicOverlapValidator {
    public boolean isTimeIntervalInTaskRange(LocalDateTime date, Duration duration, Task task){
        if (date == null || task.getStartTime() == null){
            return false;
        }

        if (date.plus(duration).isBefore(task.getStartTime()) ||
            date.plus(duration).isEqual(task.getStartTime())    ){
            return false;
        }
        if (date.isAfter(task.getStartTime().plus(task.getDuration())) ||
            date.isEqual(task.getStartTime().plus(task.getDuration()))    ){
            return false;
        }
        return true;
    }
}
