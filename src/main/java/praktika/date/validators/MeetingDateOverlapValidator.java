package praktika.date.validators;

import praktika.tasks.factories.TaskType;
import praktika.storing.whileNoDB.TaskList;
import praktika.tasks.dataStructures.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by zilka on 2016-07-26.
 */
@Component
public class MeetingDateOverlapValidator implements DateOverlapValidator {

    @Autowired
    private BasicOverlapValidator basicOverlapValidator;

    @Override
    public boolean isDateFree(LocalDateTime date, Duration duration) {
        List<Task> mainTaskList = TaskList.getTaskList();
        if (mainTaskList.isEmpty()){
            return true;
        } else {
            for (int i = 0; i < mainTaskList.size(); i++) {
                Task task = mainTaskList.get(i);
                if (task.getType() == TaskType.MEETING || task.getType() == TaskType.RESERVED_TIME){
                    if (basicOverlapValidator.isTimeIntervalInTaskRange(date,duration,task)){
                        return false;
                    }
                }
            }
            return true;
        }
    }



}

