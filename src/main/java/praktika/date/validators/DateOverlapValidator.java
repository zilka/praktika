package praktika.date.validators;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by zilka on 2016-07-26.
 */
public interface DateOverlapValidator {
    String DATE_FORMAT = "yyyy.MM.dd HH:mm";

    boolean isDateFree(LocalDateTime date, Duration duration);


}
