package praktika.date.data.manipulations;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by zilka on 2016-07-26.
 */
@Component
public class DateTransformator {
    public LocalDateTime parseDateFromString(String dateString, String dateFormat){
        if (dateString.equalsIgnoreCase("-")){
            return null;
        }
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(dateFormat);
        LocalDateTime date;
            date = LocalDateTime.parse(dateString,dateFormatter);
        return date;
    }

    public String formatStringFromDate(LocalDateTime date, String dateFormat){
        if (date == null){
            return "-";
        }
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(dateFormat);
        return date.format(dateFormatter);
    }


}
