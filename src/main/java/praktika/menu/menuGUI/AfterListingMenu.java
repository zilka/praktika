package praktika.menu.menuGUI;

import praktika.caption.CaptionService;
import praktika.menu.menuData.InputHandler;
import praktika.userInput.InputScanner;
import praktika.userInput.ConsoleInputScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by zilka on 2016-07-25.
 */
@Component
public class AfterListingMenu implements Menu {
    @Autowired
    private InputScanner inputScanner = new ConsoleInputScanner();
    @Autowired
    private CaptionService captionService;
    @Autowired
    @Qualifier("afterListingMenuHandler")
    private InputHandler inputHandler;

    public void start() throws IOException {
        String input;
        do{
            System.out.println(captionService.getCaption("AfterListingMenuCaption"));
            System.out.println(captionService.getCaption("EnterInputCaption"));
            input = inputScanner.getInput();
        } while (!inputHandler.tryHandleInput(input));
    }

}
