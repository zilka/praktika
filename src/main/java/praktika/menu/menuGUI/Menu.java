package praktika.menu.menuGUI;

import java.io.IOException;

/**
 * Created by zilvinas.vinskas on 7/22/2016.
 */
public interface Menu {
    void start() throws IOException;
}
