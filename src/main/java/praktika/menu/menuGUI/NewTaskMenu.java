package praktika.menu.menuGUI;

import praktika.userInput.InputScanner;
import praktika.caption.CaptionService;
import praktika.menu.menuData.InputHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by zilvinas.vinskas on 7/22/2016.
 */
@Component
class NewTaskMenu implements Menu {

    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private CaptionService captionService;
    @Autowired
    @Qualifier("newTaskMenuInputHandler")
    private InputHandler inputHandler;

    @Override
    public void start() throws IOException {
        String input;
        do{
            System.out.println(captionService.getCaption("NewTaskMenuCaption"));
            System.out.println(captionService.getCaption("EnterInputCaption"));
            input = inputScanner.getInput();
        } while(!inputHandler.tryHandleInput(input));
    }
}