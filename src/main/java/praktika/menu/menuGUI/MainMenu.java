package praktika.menu.menuGUI;

import praktika.caption.CaptionService;
import praktika.menu.menuData.InputHandler;
import praktika.userInput.InputScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by zilvinas.vinskas on 7/21/2016.
 */
@Component
public class MainMenu implements Menu {
    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private CaptionService captionService;
    @Autowired
    @Qualifier("mainMenuInputHandler")
    private InputHandler inputHandler;
    @Override
    public void start() throws IOException {

        do {
            System.out.println(captionService.getCaption("MainMenuCaption"));
            System.out.println(captionService.getCaption("EnterInputCaption"));
            inputHandler.tryHandleInput(inputScanner.getInput());
       } while (true);
    }
}
