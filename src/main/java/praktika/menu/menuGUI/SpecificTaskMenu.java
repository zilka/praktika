package praktika.menu.menuGUI;

import praktika.caption.CaptionService;
import praktika.menu.menuData.InputHandler;
import praktika.userInput.InputScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by zilka on 2016-07-25.
 */
@Component
public class SpecificTaskMenu implements Menu {
    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private CaptionService captionService;
    @Autowired
    @Qualifier("specificTaskMenuInputHandler")
    private InputHandler inputHandler;

    public void start() throws IOException {
        String input;
        do {
            System.out.println(captionService.getCaption("SpecificTaskMenuCaption"));
            System.out.println(captionService.getCaption("EnterInputCaption"));
            input = inputScanner.getInput();
        } while (!inputHandler.tryHandleInput(input));
    }
}
