package praktika.menu.menuGUI;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zilvinas.vinskas on 7/22/2016.
 */
@Component
public class MapOfMenusService implements InitializingBean {
    private final Map<String, Menu> menuMap = new HashMap<>();
    @Autowired
    @Qualifier("mainMenu")
    private Menu mainMenu;
    @Autowired
    @Qualifier("newTaskMenu")
    private Menu newTaskMenu;
    @Autowired
    @Qualifier("taskListMenu")
    private Menu taskListMenu;
    @Autowired
    @Qualifier("afterListingMenu")
    private Menu afterListingMenu;
    @Autowired
    @Qualifier("specificTaskMenu")
    private Menu specificTaskMenu;


    @Override
    public void afterPropertiesSet() throws Exception {
        menuMap.put("MainMenu", mainMenu);
        menuMap.put("NewTaskMenu", newTaskMenu);
        menuMap.put("TaskListMenu", taskListMenu);
        menuMap.put("AfterListingMenu", afterListingMenu);
        menuMap.put("SpecificTaskMenu", specificTaskMenu);
    }

    public Menu getMenu(final String key) {
        return menuMap.get(key);
    }
}
