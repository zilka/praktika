package praktika.menu.menuData;

import java.io.IOException;

/**
 * Created by zilvinas.vinskas on 7/21/2016.
 */
public interface InputHandler {
boolean tryHandleInput(final String input) throws IOException;

}
