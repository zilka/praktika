package praktika.menu.menuData;

import praktika.menu.menuGUI.MapOfMenusService;
import praktika.menu.menuGUI.Menu;
import praktika.caption.CaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by zilka on 2016-07-21.
 */
@Component
public class MainMenuInputHandler implements InputHandler {
    @Autowired
    private MapOfMenusService mapOfMenusService;
    @Autowired
    private CaptionService captionService;

    public boolean tryHandleInput(String input) throws IOException {
        Menu nextMenuPage;
        switch (input){
            case "1":
                nextMenuPage = mapOfMenusService.getMenu("NewTaskMenu");
                nextMenuPage.start();
                break;
            case "2":
                nextMenuPage = mapOfMenusService.getMenu("TaskListMenu");
                nextMenuPage.start();
                break;
            case "e":
                System.exit(0);
            default:
                System.out.println(captionService.getCaption("InvalidInputCaption"));
                return false;
        }
        return true;
    }
}
