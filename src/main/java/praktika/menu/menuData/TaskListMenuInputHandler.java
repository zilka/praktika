package praktika.menu.menuData;

import praktika.menu.menuGUI.MapOfMenusService;
import praktika.tasks.userOutput.TaskPrinter;
import praktika.caption.CaptionService;
import praktika.storing.whileNoDB.TaskList;
import praktika.tasks.dataStructures.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
@Component
public class TaskListMenuInputHandler implements InputHandler {
    @Autowired
    private CaptionService captionService;
    @Autowired
    private TaskList tl;
    @Autowired
    private TaskPrinter taskPrinter;
    @Autowired
    private MapOfMenusService mapOfMenusService;

    @Override
    public boolean tryHandleInput(String input) throws IOException {
        List<Task> mainTaskList = tl.getTaskList();
        switch (input){
                case "1":
                    if (!mainTaskList.isEmpty()) {
                        taskPrinter.printTasksByName(mainTaskList);
                        mapOfMenusService.getMenu("AfterListingMenu").start();
                    } else {
                        System.out.println(captionService.getCaption("EmptyList"));
                        return false;
                    }
                        break;
                case "2":
                    //TODO List tasks by START TIME
                    System.out.println(captionService.getCaption("soon"));
                    break;
                case "3":
                    //TODO List REMINDERS by START TIME
                    System.out.println(captionService.getCaption("soon"));
                    break;
                case "4":
                    //TODO List MEETINGS by START TIME
                    System.out.println(captionService.getCaption("soon"));
                    break;
                case "5":
                    //TODO List RESERVED TIME by START TIME
                    System.out.println("RESERVED TIME' list by START TIME");
                    break;
                case "b":
                    return true;
                case "e":
                    //TODO SAVE SESSION
                    System.exit(0);
                default:
                    System.out.println(captionService.getCaption("InvalidInputCaption"));
                    return false;
            }
            return true;
        }
}
