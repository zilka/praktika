package praktika.menu.menuData;

import praktika.caption.CaptionService;
import praktika.menu.menuGUI.MapOfMenusService;
import praktika.storing.manipulations.TaskFinder;
import praktika.storing.whileNoDB.TaskList;
import praktika.tasks.userOutput.TaskPrinter;
import praktika.tasks.dataStructures.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by zilvinas.vinskas on 7/26/2016.
 */
@Component
public class AfterListingMenuHandler implements InputHandler {
    @Autowired
    private CaptionService captionService;
    @Autowired
    private TaskFinder taskFinder;
    @Autowired
    private TaskList mainTaskList;
    @Autowired
    private MapOfMenusService mapOfMenusService;
    @Autowired
    TaskPrinter taskPrinter;

    @Override
    public boolean tryHandleInput(String input) throws IOException {
        switch (input) {
            case "b":
                return true;
            case "e":
                System.exit(0);
            default:
                Task task;
                task = taskFinder.findTaskById(mainTaskList.getTaskList(), Integer.parseInt(input));
                if (task == null) {
                    System.out.println(captionService.getCaption("WrongIdCaption"));
                    return false;
                } else {
                    TaskList.setFlaggedTaskId(Integer.parseInt(input));
                    taskPrinter.printTaskDetails(task);
                    mapOfMenusService.getMenu("SpecificTaskMenu").start();
                }
                return true;
        }
    }
}