package praktika.menu.menuData;

import praktika.tasks.manipulations.TaskEraser;
import praktika.tasks.manipulations.TaskUpdater;
import praktika.caption.CaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by zilvinas.vinskas on 7/26/2016.
 */
@Component
public class SpecificTaskMenuInputHandler implements InputHandler {
    @Autowired
    private CaptionService captionService;
    @Autowired
    private TaskUpdater taskUpdater;
    @Autowired
    private TaskEraser taskEraser;
    @Override
    public boolean tryHandleInput(String input) throws IOException {
        switch (input){
            case "1":
            if (taskUpdater.tryUpdateTask()){
                return true;
            } else {
                System.out.println(captionService.getCaption("ErrorUpdate"));
                return false;
            }
            case "2":
                if (taskEraser.tryEraseTask()){
                    return true;
                } else {
                    System.out.println(captionService.getCaption("ErrorDelete"));
                    return false;
                }
            case "b":
                return true;
            case "e":
                System.exit(0);
            default:
                System.out.println(captionService.getCaption("InvalidInputCaption"));
                return false;
        }
    }
}
