package praktika.menu.menuData;

import praktika.database.ToDoPersistence;
import praktika.tasks.factories.TaskFactory;
import praktika.tasks.factories.TaskType;
import praktika.tasks.dataStructures.Task;
import praktika.tasks.factories.TaskFactoryBuilder;
import praktika.caption.CaptionService;
import praktika.storing.whileNoDB.TaskList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by zilvinas.vinskas on 7/24/2016.
 */
@Component
public class NewTaskMenuInputHandler implements InputHandler {

    @Autowired
    private CaptionService captionService;
    @Autowired
    private ToDoPersistence todoDb;
    @Autowired
    private TaskFactoryBuilder taskFactoryBuilder;



    public boolean tryHandleInput(String input) throws IOException {
        Task task;
        List<Task> taskList = TaskList.getTaskList();
        TaskFactory taskFactory;
        switch (input){
            case "1":
                taskFactory = taskFactoryBuilder.makeTaskFactory(TaskType.REMINDER);
                task = taskFactory.makeTaskObject();
                if (task != null){
                    taskList.add(task);
                    todoDb.insertTask(task);
                    System.out.println(captionService.getCaption("ReminderCreated"));
                } else {
                    System.out.println(captionService.getCaption("ErrorCreate"));
                    return false;
                }
                break;

            case "2":
                taskFactory = taskFactoryBuilder.makeTaskFactory(TaskType.MEETING);
                task = taskFactory.makeTaskObject();
                if (task != null){
                    taskList.add(task);
                    todoDb.insertTask(task);
                    System.out.println(captionService.getCaption("MeetingCreated"));
                } else {
                    System.out.println(captionService.getCaption("ErrorCreate"));
                    return false;
                }
                break;

            case "3":
                taskFactory = taskFactoryBuilder.makeTaskFactory(TaskType.RESERVED_TIME);
                task = taskFactory.makeTaskObject();
                if (task != null){
                    taskList.add(task);
                    todoDb.insertTask(task);
                    System.out.println(captionService.getCaption("ReservedCreated"));
                } else {
                    System.out.println(captionService.getCaption("ErrorCreate"));
                    return false;
                }
                break;

            case "b":
                return true;
            case "e":
                //TODO SAVE SESSION
                System.exit(0);
            default:
                System.out.println(captionService.getCaption("InvalidInputCaption"));
                return false;
        }
        return true;
    }

}

//DONE

