package praktika.web.controlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import praktika.storing.whileNoDB.TaskList;
import praktika.tasks.dataStructures.Task;


import java.util.List;

/**
 * Created by zilvinas.vinskas on 8/4/2016.
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    public String home() {
        return "index";
    }

}
