package praktika.web.controlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import praktika.storing.whileNoDB.TaskList;
import praktika.tasks.dataStructures.Task;

import java.util.List;
import java.util.UUID;

/**
 * Created by zilvinas.vinskas on 8/4/2016.
 */
@Controller
public class TaskController {
    @Autowired
    TaskList taskList;

    @RequestMapping("/tasks/{id}")
    public String viewPost(@PathVariable int id, Model model) {
        model.addAttribute("task", taskList.getTaskById(id) );
        return "tasks/taskDetails";
    }

    @RequestMapping(value = "/tasks/createNewTask", method = RequestMethod.GET)
    public String newTaskPage() {
        return "tasks/createNewTask";
    }


    @RequestMapping(value = "/tasks/taskList", method = RequestMethod.GET)
    public String taskListPage(){
        return "tasks/taskList";
    }



    @ModelAttribute("allTasks")
    public List<Task> allTasks() {
        return TaskList.getTaskList();
    }
}
